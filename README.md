Google library for ZendFramework 2
==============================

Google OAuth 2.0 and API library for Zend Framework 2


Installation with Composer
-------------

Add this to your composer.json in the root of your project:

    "require": {
        "nraboy/zfgoogle": "1.0.*",
    }

Fetch the repository with composer:

    $ php composer.phar update
   

ZFGoogle Documentation
-------------

[Google OAuth 2.0](docs/Google OAuth)

[Google Play Android Developer API](docs/Android Developer API)

[Google Plus API](docs/Google Plus)

[Google Analytics API](docs/Google Analytics)

[Google URL Shortener API](docs/Google URL Shortener)


Have a question or found a bug (compliments work too)?
-------------

Email me via my website - [http://www.nraboy.com](http://www.nraboy.com/index/contact)

Tweet me on Twitter - [@nraboy](https://www.twitter.com/nraboy)


Resources
-------------

ZendFramework 2 - [http://framework.zend.com](http://framework.zend.com)