<?php

namespace Google\OAuth\Requests;

use Zend\Http\Request;
use Zend\Http\Client;

class RefreshToken {

    protected $_accessUri = "https://accounts.google.com/o/oauth2/token";
    protected $_clientId;
    protected $_clientSecret;
    protected $_grantType;
    protected $_refreshToken;

    /*
     *  Give all possible request parameters a value.  Default Google values are used whenever possible if 
     *  no other value is provided.  If too many parameters are provided that aren't needed for a Refresh 
     *  Token, then they will be ignored
     *
     *  @param    Array config    All necessary parameters for a Refresh Token call
     *  @return
     */
    public function __construct($refreshToken, $config = array()) {
        $this->setClientId(array_key_exists("client_id", $config) ? $config["client_id"] : "");
        $this->setClientSecret(array_key_exists("client_secret", $config) ? $config["client_secret"] : "");
        $this->setGrantType(array_key_exists("grant_type", $config) ? $config["grant_type"] : "refresh_token");
        $this->_refreshToken = $refreshToken;
    }

    public function setClientId($clientId) {
        $this->_clientId = $clientId;
    }

    public function setClientSecret($clientSecret) {
        $this->_clientSecret = $clientSecret;
    }

    public function setGrantType($grantType) {
        $this->_grantType = $grantType;
    }

    public function setRefreshToken($refreshToken) {
        $this->_refreshToken = $refreshToken;
    }

    public function getClientId() {
        return $this->_clientId;
    }

    public function getClientSecret() {
        return $this->_clientSecret;
    }

    public function getGrantType() {
        return $this->_grantType;
    }

    public function getRefreshToken() {
        return $this->_refreshToken;
    }

    /*
     *  Assembles all parameters to be sent to Google in a POST request
     *
     *  @param
     *  @return    Array    The POST parameters to be sent to Google
     */
    public function assembleParams() {
        $params = array(
            "client_id" => $this->getClientId(),
            "client_secret" => $this->getClientSecret(),
            "grant_type" => $this->getGrantType(),
            "refresh_token" => $this->getRefreshToken(),
        );
        return $params;
    }

    /*
     *  Make an http client call to the appropriate uri with all the correct 
     *  parameters and options included
     *
     *  @param    Array options    Http client options such as sslverifypeer
     *  @return    JSON    The response from Google
     */
    public function exchange($options = array()) {
        $client = new Client($this->_accessUri);
        $client->setMethod(Request::METHOD_POST);
        $client->setOptions($options);
        $client->setParameterPost($this->assembleParams());
        return $client->send()->getBody();
    }

}