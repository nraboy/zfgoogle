<?php

namespace Google\OAuth\Requests;

use Zend\Http\Request;
use Zend\Http\Client;

class AccessToken {

    protected $_accessUri = "https://accounts.google.com/o/oauth2/token";
    protected $_clientId;
    protected $_clientSecret;
    protected $_redirectUri;
    protected $_grantType;
    protected $_requestToken;

    public function __construct($requestToken, $config = array()) {
        $this->setClientId(array_key_exists("client_id", $config) ? $config["client_id"] : "");
        $this->setClientSecret(array_key_exists("client_secret", $config) ? $config["client_secret"] : "");
        $this->setRedirectUri(array_key_exists("redirect_uri", $config) ? $config["redirect_uri"] : "");
        $this->setGrantType(array_key_exists("grant_type", $config) ? $config["grant_type"] : "authorization_code");
        $this->_requestToken = $requestToken;
    }

    public function setClientId($clientId) {
        $this->_clientId = $clientId;
    }

    public function setClientSecret($clientSecret) {
        $this->_clientSecret = $clientSecret;
    }

    public function setRedirectUri($redirectUri) {
        $this->_redirectUri = $redirectUri;
    }

    public function setGrantType($grantType) {
        $this->_grantType = $grantType;
    }

    public function setRequestToken($requestToken) {
        $this->_requestToken = $requestToken;
    }

    public function getClientId() {
        return $this->_clientId;
    }

    public function getClientSecret() {
        return $this->_clientSecret;
    }

    public function getRedirectUri() {
        return $this->_redirectUri;
    }

    public function getGrantType() {
        return $this->_grantType;
    }

    public function getRequestToken() {
        return $this->_requestToken;
    }

    public function assembleParams() {
        $params = array(
            "client_id" => $this->getClientId(),
            "client_secret" => $this->getClientSecret(),
            "redirect_uri" => $this->getRedirectUri(),
            "grant_type" => $this->getGrantType(),
            "code" => $this->getRequestToken(),
        );
        return $params;
    }

    public function exchange($options = array()) {
        $client = new Client($this->_accessUri);
        $client->setMethod(Request::METHOD_POST);
        $client->setOptions($options);
        $client->setParameterPost($this->assembleParams());
        return $client->send()->getBody();
    }

}