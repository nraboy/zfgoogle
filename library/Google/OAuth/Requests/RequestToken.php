<?php

namespace Google\OAuth\Requests;

class RequestToken {

    protected $_requestUri = "https://accounts.google.com/o/oauth2/auth";
    protected $_clientId;
    protected $_redirectUri;
    protected $_scope;
    protected $_approvalPrompt;
    protected $_responseType;
    protected $_state;
    protected $_accessType;

    /*
     *  Give all possible request parameters a value.  Default Google values are used whenever possible if 
     *  no other value is provided.  If too many parameters are provided that aren't needed for a Request 
     *  Token, then they will be ignored
     *
     *  @param    Array config    All necessary parameters for a Request Token call
     *  @return
     */
    public function __construct($config = array()) {
        $this->setClientId(array_key_exists("client_id", $config) ? $config["client_id"] : "");
        $this->setRedirectUri(array_key_exists("redirect_uri", $config) ? $config["redirect_uri"] : "");
        $this->setScope(array_key_exists("scope", $config) ? $config["scope"] : array());
        $this->setApprovalPrompt(array_key_exists("approval_prompt", $config) ? $config["approval_prompt"] : "auto");
        $this->setResponseType(array_key_exists("response_type", $config) ? $config["response_type"] : "code");
        $this->setState(array_key_exists("state", $config) ? $config["state"] : "");
        $this->setAccessType(array_key_exists("access_type", $config) ? $config["access_type"] : "offline");
    }

    public function setClientId($clientId) {
        $this->_clientId = $clientId;
    }

    public function setRedirectUri($redirectUri) {
        $this->_redirectUri = $redirectUri;
    }

    public function setScope($scope = array()) {
        $this->_scope = $scope;
    }

    public function setApprovalPrompt($approvalPrompt) {
        $this->_approvalPrompt = $approvalPrompt;
    }

    public function setResponseType($responseType) {
        $this->_responseType = $responseType;
    }

    public function setState($state) {
        $this->_state = $state;
    }

    public function setAccessType($accessType) {
        $this->_accessType = $accessType;
    }

    public function getClientId() {
        return $this->_clientId;
    }

    public function getRedirectUri() {
        return $this->_redirectUri;
    }

    public function getScope() {
        return $this->_scope;
    }

    public function getApprovalPrompt() {
        return $this->_approvalPrompt;
    }

    public function getResponseType() {
        return $this->_responseType;
    }

    public function getState() {
        return $this->_state;
    }

    public function getAccessType() {
        return $this->_accessType;
    }

    /*
     *  Assembles all parameters to be sent to Google in a GET request
     *
     *  @param
     *  @return    String    The uri parameters to be sent to Google
     */
    public function assembleParams() {
        $params = array(
            "response_type=" . $this->getResponseType(),
            "client_id=" . $this->getClientId(),
            "redirect_uri=" . $this->getRedirectUri(),
            "scope=" . implode(" ", $this->getScope()),
            "approval_prompt=" . $this->getApprovalPrompt(),
            "state=" . $this->getState(),
            "access_type=" . $this->getAccessType(),
        );
        return implode("&", $params);
    }

    /*
     *  Generate the full Uri including parameters to be sent to Google
     *
     *  @param
     *  @return    String   The request uri with parameters appended to the end
     */
    public function generateUri() {
        return $this->_requestUri . "?" . $this->assembleParams();
    }

}