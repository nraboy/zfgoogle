<?php

namespace Google\OAuth\Tokens;

class RefreshToken {

    protected $_accessToken;
    protected $_tokenType;
    protected $_expirationTime;

    public function __construct($response = array()) {
        $this->setAccessToken(array_key_exists("access_token", $response) ? $response["access_token"] : "");
        $this->setTokenType(array_key_exists("token_type", $response) ? $response["token_type"] : "Bearer");
        $this->setExpirationTime(array_key_exists("expires_in", $response) ? $response["expires_in"] : "");
    }

    public function setAccessToken($accessToken) {
        $this->_accessToken = $accessToken;
    }

    public function setTokenType($tokenType) {
        $this->_tokenType = $tokenType;
    }

    public function setExpirationTime($expirationTime) {
        $this->_expirationTime = $expirationTime;
    }

    public function getAccessToken() {
        return $this->_accessToken;
    }

    public function getTokenType() {
        return $this->_tokenType;
    }

    public function getExpirationTime() {
        return $this->_expirationTime;
    }

}