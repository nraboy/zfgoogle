<?php

namespace Google\OAuth;

use Google\OAuth\Requests\RequestToken;
use Google\OAuth\Requests\AccessToken as AccessTokenRequest;
use Google\OAuth\Requests\RefreshToken as RefreshTokenRequest;
use Google\OAuth\Tokens\RefreshToken;
use Google\OAuth\Tokens\AccessToken;
use Zend\Json\Json;

class OAuth {

    protected $_params;     // Google OAuth parameters
    protected $_options;    // Http client options

    /*
     *  Construct OAuth handler for Google requests
     *
     *  @param    Array config    OAuth config parameters like client id, ect.
     *  @param    Array options    Http client options such as sslverifypeer
     *  @return
     */
    public function __construct($config = array(), $options = array()) {
        $this->_params = $config;
        $this->_options = $options;
    }

    /*
     *  Set the Google OAuth parameters
     *
     *  @param    Array config    Configuration parameters such as client_id or redirect_uri
     *  @return
     */
    public function setConfig($config) {
        $this->_params = $config;
    }

    /*
     *  Set the http client options
     *
     *  @param    Array options    Client options such as sslverifypeer
     *  @return
     */
    public function setOptions($options) {
        $this->_options = $options;
    }

    /*
     *  Get the current Google OAuth parameters
     *
     *  @param
     *  @return    Array
     */
    public function getConfig() {
        return $this->_params;
    }

    /*
     *  Get the current http client options
     *
     *  @param
     *  @return    Array
     */
    public function getOptions() {
        return $this->_options;
    }

    /*
     *  Get the request token
     *
     *  @param
     *  @return
     */
    public function getRequestToken() {
        $requestToken = new RequestToken($this->_params);
        header("Location: " . $requestToken->generateUri());
        exit(1);
    }

    /*
     *  Get the access token using the latest request token
     *
     *  @param    String requestToken    The request token received from OAuth::getRequestToken()
     *  @return   Tokens\AccessToken    access_token, refresh_token, and other response information
     */
    public function getAccessToken($requestToken) {
        $accessToken = new AccessTokenRequest($requestToken, $this->_params);
        $response = $accessToken->exchange($this->_options);
        return new AccessToken(Json::decode($response, Json::TYPE_ARRAY));
    }


    /*
     *  Refresh the access token without making a request for a new access token.  This is normally 
     *  used because the currect access token has expired
     *
     *  @param    String refreshToken    The refresh token received from Tokens\AccessToken::getRefreshToken()
     *  @return    Tokens\RefreshToken    access_token, expires_in, and other response information
     */
    public function refreshAccessToken($refreshToken) {
        $response = (new RefreshTokenRequest($refreshToken, $this->_params))->exchange($this->_options);
        return new RefreshToken(Json::decode($response, Json::TYPE_ARRAY));
    }

}