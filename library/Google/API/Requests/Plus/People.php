<?php

namespace Google\API\Requests\Plus;

use Zend\Http\Request;
use Zend\Http\Client;

class People {

    protected $_requestUri = "https://www.googleapis.com/plus/v1/people";
    protected $_options;
    protected $_accessToken;
    protected $_userId;
    protected $_collection;
    protected $_query;
    protected $_language;
    protected $_maxResults;
    protected $_pageToken;
    protected $_orderBy;

    public function __construct($accessToken, $params = array()) {
        $this->setUserId(array_key_exists("user_id", $params) ? $params["user_id"] : "me");
        $this->setCollection(array_key_exists("collection", $params) ? $params["collection"] : "connected");
        $this->setQuery(array_key_exists("query", $params) ? $params["query"] : "");
        $this->setLanguage(array_key_exists("language", $params) ? $params["language"] : "en-US");
        $this->setMaxResults(array_key_exists("max_results", $params) ? $params["max_results"] : "25");
        $this->setPageToken(array_key_exists("page_token", $params) ? $params["page_token"] : "");
        $this->setOrderBy(array_key_exists("order_by", $params) ? $params["order_by"] : "alphabetical");
        $this->_accessToken = $accessToken;
    }

    public function setUserId($userId) {
        $this->_userId = $userId;
    }

    public function setCollection($collection) {
        $this->_collection = $collection;
    }

    public function setQuery($query) {
        $this->_query = $query;
    }

    public function setLanguage($language) {
        $this->_language = $language;
    }

    public function setMaxResults($maxResults) {
        $this->_maxResults = $maxResults;
    }

    public function setPageToken($pageToken) {
        $this->_pageToken = $pageToken;
    }

    public function setOrderBy($orderBy) {
        $this->_orderBy = $orderBy;
    }

    public function getUserId() {
        return $this->_userId;
    }

    public function getCollection() {
        return $this->_collection;
    }

    public function getQuery() {
        return $this->_query;
    }

    public function getLanguage() {
        return $this->_language;
    }

    public function getMaxResults() {
        return $this->_maxResults;
    }

    public function getPageToken() {
        return $this->_pageToken;
    }

    public function getOrderBy() {
        return $this->_orderBy;
    }

    public function assembleParamsForSearch() {
        $params = array(
            "query=" . $this->getQuery(),
            "language=" . $this->getLanguage(),
            "maxResults=" . $this->getMaxResults(),
            "pageToken=" . $this->getPageToken(),
        );
        return implode("&", $params);
    }

    public function assembleParamsForList() {
        $pathParams = array(
            $this->getUserId(),
            "people/" . $this->getCollection(),
        );
        $queryParams = array(
            "maxResults=" . $this->getMaxResults(),
            "orderBy=" . $this->getOrderBy(),
            "pageToken=" . $this->getPageToken(),
        );
        return implode("/", $pathParams) . "?" . implode("&", $queryParams);
    }

    public function searchPeople($options = array()) {
        $client = new Client($this->_requestUri . "?" . $this->assembleParamsForSearch());
        $client->setMethod(Request::METHOD_GET);
        $client->setOptions($options);
        $client->setParameterGet(array("access_token" => $this->_accessToken));
        return $client->send()->getBody();
    }

    public function listPeople($options = array()) {
        $client = new Client($this->_requestUri . "/" . $this->assembleParamsForList());
        $client->setMethod(Request::METHOD_GET);
        $client->setOptions($options);
        $client->setParameterGet(array("access_token" => $this->_accessToken));
        return $client->send()->getBody();
    }

}