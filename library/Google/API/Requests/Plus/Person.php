<?php

namespace Google\API\Requests\Plus;

use Zend\Http\Request;
use Zend\Http\Client;

class Person {

    protected $_requestUri = "https://www.googleapis.com/plus/v1";
    protected $_options;
    protected $_accessToken;
    protected $_userId;

    public function __construct($accessToken, $params = array()) {
        $this->setUserId(array_key_exists("user_id", $params) ? $params["user_id"] : "me");
        $this->_accessToken = $accessToken;
    }

    public function setUserId($userId) {
        $this->_userId = $userId;
    }

    public function getUserId() {
        return $this->_userId;
    }

    public function assembleParamsGet() {
        $params = array(
            "people/" . $this->getUserId(),
        );
        return implode("/", $params);
    }

    public function get($options = array()) {
        $client = new Client($this->_requestUri . "/" . $this->assembleParamsGet());
        $client->setMethod(Request::METHOD_GET);
        $client->setOptions($options);
        $client->setParameterGet(array("access_token" => $this->_accessToken));
        return $client->send()->getBody();
    }

}