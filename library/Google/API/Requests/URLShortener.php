<?php

namespace Google\API\Requests;

use Zend\Http\Request;
use Zend\Http\Client;
use Zend\Json\Json;

class URLShortener {

    protected $_requestUri = "https://www.googleapis.com/urlshortener/v1/url";
    protected $_options;
    protected $_accessToken;
    protected $_shortUrl;
    protected $_longUrl;
    protected $_projection;

    public function __construct($accessToken, $params = array()) {
        $this->setShortUrl(array_key_exists("short_url", $params) ? $params["short_url"] : "");
        $this->setLongUrl(array_key_exists("long_url", $params) ? $params["long_url"] : "");
        $this->setProjection(array_key_exists("projection", $params) ? $params["projection"] : "");
        $this->_accessToken = $accessToken;
    }

    public function setShortUrl($shortUrl) {
        $this->_shortUrl = $shortUrl;
    }

    public function setLongUrl($longUrl) {
        $this->_longUrl = $longUrl;
    }

    public function setProjection($projection) {
        $this->_projection = $projection;
    }

    public function getShortUrl() {
        return $this->_shortUrl;
    }

    public function getLongUrl() {
        return $this->_longUrl;
    }

    public function getProjection() {
        return $this->_projection;
    }

    public function assembleParamsForShorten() {
        $params = array(
            "longUrl" => $this->getLongUrl(),
        );
        return $params;
    }

    public function assembleParamsForExpand() {
        $params = array(
            "access_token" => $this->_accessToken,
            "shortUrl" => $this->getShortUrl(),
        );
        if($this->getProjection() != "") { $params["projection"] = $this->getProjection(); }
        return $params;
    }

    public function shorten($options = array()) {
        $client = new Client($this->_requestUri . "?access_token=" . $this->_accessToken);
        $client->setMethod(Request::METHOD_POST);
        $client->setEncType("application/json");
        $client->setOptions($options);
        $client->setRawBody(Json::encode($this->assembleParamsForShorten()));
        return $client->send()->getBody();
    }

    public function expand($options = array()) {
        $client = new Client($this->_requestUri);
        $client->setMethod(Request::METHOD_GET);
        $client->setOptions($options);
        $client->setParameterGet($this->assembleParamsForExpand());
        return $client->send()->getBody();
    }

}