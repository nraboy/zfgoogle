<?php

namespace Google\API\Requests;

use Zend\Http\Request;
use Zend\Http\Client;

class Analytics {

    protected $_requestUri = "https://www.googleapis.com/analytics/v3/data/ga";
    protected $_options;
    protected $_accessToken;
    protected $_ids;
    protected $_metrics;
    protected $_startDate;
    protected $_endDate;
    protected $_dimensions;
    protected $_sort;
    protected $_filters;
    protected $_segment;
    protected $_maxResults;

    public function __construct($accessToken, $params = array()) {
        $this->setIds(array_key_exists("ids", $params) ? $params["ids"] : "");
        $this->setMetrics(array_key_exists("metrics", $params) ? $params["metrics"] : "");
        $this->setStartDate(array_key_exists("start_date", $params) ? $params["start_date"] : "");
        $this->setEndDate(array_key_exists("end_date", $params) ? $params["end_date"] : "");
        $this->setDimensions(array_key_exists("dimensions", $params) ? $params["dimensions"] : "");
        $this->setSort(array_key_exists("sort", $params) ? $params["sort"] : "");
        $this->setFilters(array_key_exists("filters", $params) ? $params["filters"] : "");
        $this->setSegment(array_key_exists("segment", $params) ? $params["segment"] : "");
        $this->setMaxResults(array_key_exists("max_results", $params) ? $params["max_results"] : "25");
        $this->_accessToken = $accessToken;
    }

    public function setIds($ids) {
        $this->_ids = $ids;
    }

    public function setMetrics($metrics) {
        $this->_metrics = $metrics;
    }

    public function setStartDate($startDate) {
        $this->_startDate = $startDate;
    }

    public function setEndDate($endDate) {
        $this->_endDate = $endDate;
    }

    public function setDimensions($dimensions) {
        $this->_dimensions = $dimensions;
    }

    public function setSort($sort) {
        $this->_sort = $sort;
    }

    public function setFilters($filters) {
        $this->_filters = $filters;
    }

    public function setSegment($segment) {
        $this->_segment = $segment;
    }

    public function setMaxResults($maxResults) {
        $this->_maxResults = $maxResults;
    }

    public function getIds() {
        return $this->_ids;
    }

    public function getMetrics() {
        return $this->_metrics;
    }

    public function getStartDate() {
        return $this->_startDate;
    }

    public function getEndDate() {
        return $this->_endDate;
    }

    public function getDimensions() {
        return $this->_dimensions;
    }

    public function getSort() {
        return $this->_sort;
    }

    public function getFilters() {
        return $this->_filters;
    }

    public function getSegment() {
        return $this->_segment;
    }

    public function getMaxResults() {
        return $this->_maxResults;
    }

    public function assembleParamsGet() {
        $params = array(
            "ids=" . $this->getIds(),
            "metrics=" . $this->getMetrics(),
            "start-date=" . $this->getStartDate(),
            "end-date=" . $this->getEndDate(),
            "dimensions=" . $this->getDimensions(),
            "max-results=" . $this->getMaxResults(),
        );
        if($this->getSort() != "") {
            $params[] = "sort=" . $this->getSort();
        }
        if($this->getFilters() != "") {
            $params[] = "filters=" . $this->getFilters();
        }
        if($this->getSegment() != "") {
            $params[] = "segment=" . $this->getSegment();
        }
        return implode("&", $params);
    }

    public function exchange($options = array()) {
        $client = new Client($this->_requestUri . "?" . $this->assembleParamsGet());
        $client->setMethod(Request::METHOD_GET);
        $client->setOptions($options);
        $client->setParameterGet(array("access_token" => $this->_accessToken));
        return $client->send()->getBody();
    }

}