<?php

namespace Google\API\Requests;

use Zend\Http\Request;
use Zend\Http\Client;
//use Zend\Json\Json;

class AndroidDeveloper {

    protected $_requestUri = "https://www.googleapis.com/androidpublisher/v1";
    protected $_options;
    protected $_accessToken;
    protected $_packageName;
    protected $_subscriptionId;
    protected $_token;

    public function __construct($accessToken, $params = array()) {
        $this->setPackageName(array_key_exists("package_name", $params) ? $params["package_name"] : "");
        $this->setSubscriptionId(array_key_exists("subscription_id", $params) ? $params["subscription_id"] : "");
        $this->setToken(array_key_exists("token", $params) ? $params["token"] : "");
        $this->_accessToken = $accessToken;
    }

    public function setPackageName($packageName) {
        $this->_packageName = $packageName;
    }

    public function setSubscriptionId($subscriptionId) {
        $this->_subscriptionId = $subscriptionId;
    }

    public function setToken($token) {
        $this->_token = $token;
    }

    public function getPackageName() {
        return $this->_packageName;
    }

    public function getSubscriptionId() {
        return $this->_subscriptionId;
    }

    public function getToken() {
        return $this->_token;
    }

    public function assembleParamsGet() {
        $params = array(
            "applications/" . $this->getPackageName(),
            "subscriptions/" . $this->getSubscriptionId(),
            "purchases/" . $this->getToken(),
        );
        return implode("/", $params);
    }

    public function getPurchase($options = array()) {
        $client = new Client($this->_requestUri . "/" . $this->assembleParamsGet());
        $client->setMethod(Request::METHOD_GET);
        $client->setOptions($options);
        $client->setParameterGet(array("access_token" => $this->_accessToken));
        return $client->send()->getBody();
    }

    public function cancelPurchase($options = array()) {
        $client = new Client($this->_requestUri . "/" . $this->assembleParamsGet() . "/cancel");
        $client->setMethod(Request::METHOD_GET);
        $client->setOptions($options);
        $client->setParameterGet(array("access_token" => $this->_accessToken));
        return $client->send()->getBody();
    }

}