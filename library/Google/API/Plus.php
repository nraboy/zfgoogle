<?php

namespace Google\API;

use Google\API\Requests\Plus\Person as PersonRequest;
use Google\API\Responses\Plus\Person as PersonResponse;
use Google\API\Requests\Plus\People as PeopleRequest;
use Google\API\Responses\Plus\People as PeopleResponse;
use Zend\Json\Json;

class Plus {

    protected $_options;
    protected $_accessToken;

    public function __construct($accessToken, $options = array()) {
        $this->_accessToken = $accessToken;
        $this->_options = $options;
    }

    public function getPerson($params = array()) {
        $response = (new PersonRequest($this->_accessToken, $params))->get($this->_options);
        return new PersonResponse(Json::decode($response, Json::TYPE_ARRAY));
    }

    public function searchPeople($params = array()) {
        $response = (new PeopleRequest($this->_accessToken, $params))->searchPeople($this->_options);
        return new PeopleResponse(Json::decode($response, Json::TYPE_ARRAY));
    }

    public function listPeople($params = array()) {
        $response = (new PeopleRequest($this->_accessToken, $params))->listPeople($this->_options);
        return new PeopleResponse(Json::decode($response, Json::TYPE_ARRAY));
    }

}