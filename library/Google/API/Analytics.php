<?php

namespace Google\API;

use Google\API\Requests\Analytics as AnalyticsRequest;
use Google\API\Responses\Analytics as AnalyticsResponse;
use Zend\Json\Json;

class Analytics {

    protected $_options;
    protected $_accessToken;

    public function __construct($accessToken, $options = array()) {
        $this->_accessToken = $accessToken;
        $this->_options = $options;
    }

    public function query($params = array()) {
        $response = (new AnalyticsRequest($this->_accessToken, $params))->exchange($this->_options);
        return new AnalyticsResponse(Json::decode($response, Json::TYPE_ARRAY));
    }

}