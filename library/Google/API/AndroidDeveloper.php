<?php

namespace Google\API;

use Google\API\Requests\AndroidDeveloper as AndroidDeveloperRequest;
use Google\API\Responses\AndroidDeveloper as AndroidDeveloperResponse;
use Zend\Json\Json;

class AndroidDeveloper {

    protected $_options;
    protected $_accessToken;

    public function __construct($accessToken, $options = array()) {
        $this->_accessToken = $accessToken;
        $this->_options = $options;
    }

    public function getPurchase($params = array()) {
        $response = (new AndroidDeveloperRequest($this->_accessToken, $params))->getPurchase($this->_options);
        return new AndroidDeveloperResponse(Json::decode($response, Json::TYPE_ARRAY));
    }

    public function cancelPurchase($params = array()) {
        $response = (new AndroidDeveloperRequest($this->_accessToken, $this->_params))->cancelPurchase($this->_options);
        return new AndroidDeveloperResponse(Json::decode($response, Json::TYPE_ARRAY));
    }

}