<?php

namespace Google\API\Responses\Plus;

class Person {

    protected $_kind;
    protected $_id;
    protected $_skills;
    protected $_gender;
    protected $_occupation;
    protected $_nickname;
    protected $_tagline;
    protected $_aboutMe;
    protected $_currentLocation;
    protected $_url;
    protected $_displayName;
    protected $_etag;
    protected $_birthday;
    protected $_braggingRights;
    protected $_imageUrl;
    protected $_errorCode;
    protected $_errorMessage;

    public function __construct($response = array()) {
        $this->setKind(array_key_exists("kind", $response) ? $response["kind"] : "plus#person");
        $this->setId(array_key_exists("id", $response) ? $response["id"] : "");
        $this->setSkills(array_key_exists("skills", $response) ? $response["skills"] : "");
        $this->setGender(array_key_exists("gender", $response) ? $response["gender"] : "");
        $this->setOccupation(array_key_exists("occupation", $response) ? $response["occupation"] : "");
        $this->setNickname(array_key_exists("nickname", $response) ? $response["nickname"] : "");
        $this->setTagline(array_key_exists("tagline", $response) ? $response["tagline"] : "");
        $this->setAboutMe(array_key_exists("aboutMe", $response) ? $response["aboutMe"] : "");
        $this->setCurrentLocation(array_key_exists("currentLocation", $response) ? $response["currentLocation"] : "");
        $this->setUrl(array_key_exists("url", $response) ? $response["url"] : "");
        $this->setDisplayName(array_key_exists("displayName", $response) ? $response["displayName"] : "");
        $this->setEtag(array_key_exists("etag", $response) ? $response["etag"] : "");
        $this->setBirthday(array_key_exists("birthday", $response) ? $response["birthday"] : "");
        $this->setBraggingRights(array_key_exists("braggingRights", $response) ? $response["braggingRights"] : "");
        $this->setImageUrl(array_key_exists("image", $response) ? $response["image"]["url"] : "");
        $this->setErrorCode(array_key_exists("error", $response) ? $response["error"]["code"] : "");
        $this->setErrorMessage(array_key_exists("error", $response) ? $response["error"]["message"] : "");
    }

    public function setKind($kind) {
        $this->_kind = $kind;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function setSkills($skills) {
        $this->_skills = $skills;
    }

    public function setGender($gender) {
        $this->_gender = $gender;
    }

    public function setOccupation($occupation) {
        $this->_occupation = $occupation;
    }

    public function setNickname($nickname) {
        $this->_nickname = $nickname;
    }

    public function setTagline($tagline) {
        $this->_tagline = $tagline;
    }

    public function setAboutMe($aboutMe) {
        $this->_aboutMe = $aboutMe;
    }

    public function setCurrentLocation($currentLocation) {
        $this->_currentLocation = $currentLocation;
    }

    public function setUrl($url) {
        $this->_url = $url;
    }

    public function setDisplayName($displayName) {
        $this->_displayName = $displayName;
    }

    public function setEtag($etag) {
        $this->_etag = $etag;
    }

    public function setBirthday($birthday) {
        $this->_birthday = $birthday;
    }

    public function setBraggingRights($braggingRights) {
        $this->_braggingRights = $braggingRights;
    }

    public function setImageUrl($imageUrl) {
        $this->_imageUrl = $imageUrl;
    }

    public function setErrorCode($errorCode) {
        $this->_errorCode = $errorCode;
    }

    public function setErrorMessage($errorMessage) {
        $this->_errorMessage = $errorMessage;
    }

    public function getKind() {
        return $this->_kind;
    }

    public function getId() {
        return $this->_id;
    }

    public function getSkills() {
        return $this->_skills;
    }

    public function getGender() {
        return $this->_gender;
    }

    public function getOccupation() {
        return $this->_occupation;
    }

    public function getNickname() {
        return $this->_nickname;
    }

    public function getTagline() {
        return $this->_tagline;
    }

    public function getAboutMe() {
        return $this->_aboutMe;
    }

    public function getCurrentLocation() {
        return $this->_currentLocation;
    }

    public function getUrl() {
        return $this->_url;
    }

    public function getDisplayName() {
        return $this->_displayName;
    }

    public function getEtag() {
        return $this->_etag;
    }

    public function getBirthday() {
        return $this->_birthday;
    }

    public function getBraggingRights() {
        return $this->_braggingRights;
    }

    public function getImageUrl() {
        return $this->_imageUrl;
    }

    public function getErrorCode() {
        return $this->_errorCode;
    }

    public function getErrorMessage() {
        return $this->_errorMessage;
    }

}