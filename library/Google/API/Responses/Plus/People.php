<?php

namespace Google\API\Responses\Plus;

use Google\API\Responses\Plus\Person;

class People {

    protected $_kind;
    protected $_items;
    protected $_selfLink;
    protected $_title;
    protected $_nextPageToken;
    protected $_etag;
    protected $_totalItems;
    protected $_errorCode;
    protected $_errorMessage;

    public function __construct($response = array()) {
        $this->setKind(array_key_exists("kind", $response) ? $response["kind"] : "plus#peopleFeed");
        $this->setItems(array_key_exists("items", $response) ? $response["items"] : "");
        $this->setSelfLink(array_key_exists("selfLink", $response) ? $response["selfLink"] : "");
        $this->setTitle(array_key_exists("title", $response) ? $response["title"] : "");
        $this->setNextPageToken(array_key_exists("nextPageToken", $response) ? $response["nextPageToken"] : "");
        $this->setEtag(array_key_exists("etag", $response) ? $response["etag"] : "");
        $this->setTotalItems(array_key_exists("totalItems", $response) ? $response["totalItems"] : "");
        $this->setErrorCode(array_key_exists("error", $response) ? $response["error"]["code"] : "");
        $this->setErrorMessage(array_key_exists("error", $response) ? $response["error"]["message"] : "");
    }

    public function setKind($kind) {
        $this->_kind = $kind;
    }

    public function setItems($items) {
        $this->_items = array();
        foreach($items as $item) {
            $this->_items[] = new Person($item);
        }
    }

    public function setSelfLink($selfLink) {
        $this->_selfLink = $selfLink;
    }

    public function setTitle($title) {
        $this->_title = $title;
    }

    public function setNextPageToken($nextPageToken) {
        $this->_nextPageToken = $nextPageToken;
    }

    public function setEtag($etag) {
        $this->_etag = $etag;
    }

    public function setTotalItems($totalItems) {
        $this->_totalItems = $totalItems;
    }

    public function setErrorCode($errorCode) {
        $this->_errorCode = $errorCode;
    }

    public function setErrorMessage($errorMessage) {
        $this->_errorMessage = $errorMessage;
    }

    public function getKind() {
        return $this->_kind;
    }

    public function getItems() {
        return $this->_items;
    }

    public function getSelfLink() {
        return $this->_selfLink;
    }

    public function getTitle() {
        return $this->_title;
    }

    public function getNextPageToken() {
        return $this->_nextPageToken;
    }

    public function getEtag() {
        return $this->_etag;
    }

    public function getTotalItems() {
        return $this->_totalItems;
    }

    public function getErrorCode() {
        return $this->_errorCode;
    }

    public function getErrorMessage() {
        return $this->_errorMessage;
    }

}