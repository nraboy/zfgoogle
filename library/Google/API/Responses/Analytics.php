<?php

namespace Google\API\Responses;

class Analytics {

    protected $_kind;
    protected $_totalResults;
    protected $_columns;
    protected $_rows;
    protected $_errorCode;
    protected $_errorMessage;

    public function __construct($response = array()) {
        $this->setKind(array_key_exists("kind", $response) ? $response["kind"] : "analytics#gaData");
        $this->setTotalResults(array_key_exists("totalResults", $response) ? $response["totalResults"] : "");
        $this->setColumns(array_key_exists("columnHeaders", $response) ? $response["columnHeaders"] : "");
        $this->setRows(array_key_exists("rows", $response) ? $response["rows"] : "");
        $this->setErrorCode(array_key_exists("error", $response) ? $response["error"]["code"] : "");
        $this->setErrorMessage(array_key_exists("error", $response) ? $response["error"]["message"] : "");
    }

    public function setKind($kind) {
        $this->_kind = $kind;
    }

    public function setTotalResults($totalResults) {
        $this->_totalResults = $totalResults;
    }

    public function setColumns($columns) {
        $this->_columns = array();
        foreach($columns as $column) {
            $this->_columns[] = $column["name"];
        }
    }

    public function setRows($rows) {
        $this->_rows = array();
        foreach($rows as $row) {
            $formatRow = array();
            foreach($row as $c => $v) {
                $formatRow[$this->_columns[$c]] = $v;
            }
            $this->_rows[] = $formatRow;
        }
    }

    public function setErrorCode($errorCode) {
        $this->_errorCode = $errorCode;
    }

    public function setErrorMessage($errorMessage) {
        $this->_errorMessage = $errorMessage;
    }

    public function getKind() {
        return $this->_kind;
    }

    public function getTotalResults() {
        return $this->_totalResults;
    }

    public function getColumns() {
        return $this->_columns;
    }

    public function getRows() {
        return $this->_rows;
    }

    public function getErrorCode() {
        return $this->_errorCode;
    }

    public function getErrorMessage() {
        return $this->_errorMessage;
    }

}