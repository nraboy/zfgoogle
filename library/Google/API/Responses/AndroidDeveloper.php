<?php

namespace Google\API\Responses;

class AndroidDeveloper {

    protected $_kind;
    protected $_initiationTimestamp;
    protected $_expirationTimestamp;
    protected $_autoRenew;
    protected $_errorCode;
    protected $_errorMessage;

    public function __construct($response = array()) {
        $this->setKind(array_key_exists("kind", $response) ? $response["kind"] : "androidpublisher#subscriptionPurchase");
        $this->setInitiationTimestamp(array_key_exists("initiationTimestampMsec", $response) ? $response["initiationTimestampMsec"] : "");
        $this->setExpirationTimestamp(array_key_exists("validUntilTimestampMsec", $response) ? $response["validUntilTimestampMsec"] : "");
        $this->setAutoRenew(array_key_exists("autoRenewing", $response) ? $response["autoRenewing"] : "");
        $this->setErrorCode(array_key_exists("error", $response) ? $response["error"]["code"] : "");
        $this->setErrorMessage(array_key_exists("error", $response) ? $response["error"]["message"] : "");
    }

    public function setKind($kind) {
        $this->_kind = $kind;
    }

    public function setInitiationTimestamp($initiationTimestamp) {
        $this->_initiationTimestamp = $initiationTimestamp;
    }

    public function setExpirationTimestamp($expirationTimestamp) {
        $this->_expirationTimestamp = $expirationTimestamp;
    }

    public function setAutoRenew($autoRenew) {
        $this->_autoRenew = $autoRenew;
    }

    public function setErrorCode($errorCode) {
        $this->_errorCode = $errorCode;
    }

    public function setErrorMessage($errorMessage) {
        $this->_errorMessage = $errorMessage;
    }

    public function getKind() {
        return $this->_kind;
    }

    public function getInitiationTimestamp() {
        return $this->_initiationTimestamp;
    }

    public function getExpirationTimestamp() {
        return $this->_expirationTimestamp;
    }

    public function getAutoRenew() {
        return $this->_autoRenew;
    }

    public function getErrorCode() {
        return $this->_errorCode;
    }

    public function getErrorMessage() {
        return $this->_errorMessage;
    }

}