<?php

namespace Google\API\Responses;

class URLShortener {

    protected $_kind;
    protected $_id;
    protected $_longUrl;
    protected $_status;
    protected $_created;
    protected $_errorCode;
    protected $_errorMessage;

    public function __construct($response = array()) {
        $this->setKind(array_key_exists("kind", $response) ? $response["kind"] : "urlshortener#url");
        $this->setId(array_key_exists("id", $response) ? $response["id"] : "");
        $this->setLongUrl(array_key_exists("longUrl", $response) ? $response["longUrl"] : "");
        $this->setStatus(array_key_exists("status", $response) ? $response["status"] : "");
        $this->setCreated(array_key_exists("created", $response) ? $response["created"] : "");
        $this->setErrorCode(array_key_exists("error", $response) ? $response["error"]["code"] : "");
        $this->setErrorMessage(array_key_exists("error", $response) ? $response["error"]["message"] : "");
    }

    public function setKind($kind) {
        $this->_kind = $kind;
    }

    public function setId($id) {
        $this->_id = $id;
    }

    public function setLongUrl($longUrl) {
        $this->_longUrl = $longUrl;
    }

    public function setStatus($status) {
        $this->_status = $status;
    }

    public function setCreated($created) {
        $this->_created = $created;
    }

    public function setErrorCode($errorCode) {
        $this->_errorCode = $errorCode;
    }

    public function setErrorMessage($errorMessage) {
        $this->_errorMessage = $errorMessage;
    }

    public function getKind() {
        return $this->_kind;
    }

    public function getId() {
        return $this->_id;
    }

    public function getLongUrl() {
        return $this->_longUrl;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function getCreated() {
        return $this->_created;
    }

    public function getErrorCode() {
        return $this->_errorCode;
    }

    public function getErrorMessage() {
        return $this->_errorMessage;
    }

}