<?php

namespace Google\API;

use Google\API\Requests\URLShortener as URLShortenerRequest;
use Google\API\Responses\URLShortener as URLShortenerResponse;
use Zend\Json\Json;

class URLShortener {

    protected $_options;
    protected $_accessToken;

    public function __construct($accessToken, $options = array()) {
        $this->_accessToken = $accessToken;
        $this->_options = $options;
    }

    public function shorten($params = array()) {
        $response = (new URLShortenerRequest($this->_accessToken, $params))->shorten($this->_options);
        return new URLShortenerResponse(Json::decode($response, Json::TYPE_ARRAY));
    }

    public function expand($params = array()) {
        $response = (new URLShortenerRequest($this->_accessToken, $params))->expand($this->_options);
        return new URLShortenerResponse(Json::decode($response, Json::TYPE_ARRAY));
    }

}