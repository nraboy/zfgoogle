Google URL Shortener API
==============================

Google URL Shortener API classes for ZendFramework 2


Required OAuth Scopes
-------------

```
#!text
https://www.googleapis.com/auth/urlshortener
```


ZFGoogle Function to API Mapping
-------------

```
#!text
| ZFGoogle Function                  | Google API Command |
| ---------------------------------- | ------------------ |
| Google\API\URLShortener::shorten() | insert             |
| Google\API\URLShortener::expand()  | get                |
```


Possible API Parameters
-------------

```
#!php
protected $_params = array(
    "long_url" => "{URL_TO_BE_SHORTENED}",
    "short_url" => "{URL_TO_BE_EXPANDED}",
    "projection" => "FULL|ANALYTICS_CLICKS|ANALYTICS|TOP_STRINGS"
);
```

For help choosing the appropriate parameters for each API call, visit the official 
Google documentation found in the resources of this page


Expected Results for URLShortener::shorten($params)
-------------

URLShortener::shorten($params) will return a Google\API\Responses\URLShortener object
with the following methods:

```
#!text
Google\API\Responses\URLShortener::getKind();
Google\API\Responses\URLShortener::getId();
Google\API\Responses\URLShortener::getLongUrl();
Google\API\Responses\URLShortener::getErrorCode();
Google\API\Responses\URLShortener::getErrorMessage();
```

Expected Results for URLShortener::expand($params)
-------------

URLShortener::expand($params) will return a Google\API\Responses\URLShortener object
with the following methods:

```
#!text
Google\API\Responses\URLShortener::getKind();
Google\API\Responses\URLShortener::getId();
Google\API\Responses\URLShortener::getLongUrl();
Google\API\Responses\URLShortener::getStatus();
Google\API\Responses\URLShortener::getErrorCode();
Google\API\Responses\URLShortener::getErrorMessage();
```


Example
-------------

    use Google\API\URLShortener;

    class MyAPIController extends AbstractActionController {

        protected $_options;
        protected $_params;

        public function __construct() {
            $this->_params = array(
                "longUrl" => "http://code.nraboy.com/zfgoogle", 
            );
            $this->_options = array(
                "sslverifypeer" => false
            );
        }

        public function indexAction() {
            $api = new URLShortener($accessToken, $this->_options);
            $response = $api->shorten($this->_params);
            // echo $response->getId();
            // echo $response->getLongUrl();

            $response = $api->expand(array("short_url" => "http://goo.gl/fbsS"));
            // echo $response->getLongUrl();
            // echo $response->getStatus();
        }
        
    }


Resources
-------------

Google URL Shortener API Documentation - [https://developers.google.com/url-shortener/v1/getting_started](https://developers.google.com/url-shortener/v1/getting_started)