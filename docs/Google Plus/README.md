Google Plus API
==============================

Google Plus API classes for ZendFramework 2


Required OAuth Scopes
-------------

```
#!text
https://www.googleapis.com/auth/plus.login
https://www.googleapis.com/auth/plus.me
```


ZFGoogle Function to API Mapping
-------------

```
#!text
| ZFGoogle Function               | Google API Command |
| ------------------------------- | ------------------ |
| Google\API\Plus::getPerson()    | get                |
| Google\API\Plus::searchPerson() | search             |
| Google\API\Plus::listPerson()   | list               |
```


Possible API Parameters
-------------

```
#!php
protected $_params = array(
    "user_id" => "{USER_ID}|me",
    "query" => "{SEARCH_TERM_HERE}",
    "max_results" => "{COUNT}",
    "language" => "{LANGUAGE_CODE_HERE}",
    "page_token" => "{NEXT_PAGE_TOKEN_HERE}",
    "order_by" => "alphabetical|best",
    "collection" => "connected|visible",
);
```

For help choosing the appropriate parameters for each API call, visit the official 
Google documentation found in the resources of this page


Expected Results for Plus::getPerson($params)
-------------

Plus::getPerson($params) will return a Google\API\Responses\Plus\Person object
with the following methods:

```
#!text
Google\API\Responses\Plus\Person::getUrl();
Google\API\Responses\Plus\Person::getOccupation();
Google\API\Responses\Plus\Person::getGender();
```

There are many more functions in the Person object.  A rough idea of what exists can be found 
in the Google documentation at [https://developers.google.com/+/api/latest/people#resource](https://developers.google.com/+/api/latest/people#resource)


Expected Results for Plus::searchPeople($params) and Plus::listPeople($params)
-------------

Plus::searchPeople($params) and Plus::listPeople($params) will return a Google\API\Responses\Plus\People object
with the following methods:

```
#!text
Google\API\Responses\Plus\People::getKind();
Google\API\Responses\Plus\People::getTitle();
Google\API\Responses\Plus\People::getNextPageToken();
Google\API\Responses\Plus\People::getEtag();
Google\API\Responses\Plus\People::getSelfLink();
Google\API\Responses\Plus\People::getTotalItems();
Google\API\Responses\Plus\People::getItems();
```

Google\API\Responses\Plus\People::getItems() will return an array of Google\API\Responses\Plus\Person however, not all fields will be filled like when 
calling Plus::getPerson($params)


Example
-------------

    use Google\API\Plus;

    class MyAPIController extends AbstractActionController {

        protected $_options;

        public function __construct() {
            $this->_options = array(
                "sslverifypeer" => false
            );
        }

        public function indexAction() {
            $api = new Plus($accessToken, $this->_options);
            $response = $api->getPerson(array("user_id" => "me"));
            // echo $response->getGender();
            // echo $response->getUrl();
            // echo $response->getOccupation();

            $response = $api->searchPeople(array("query" => "raboy", "language" => "en-US", "max_results" => 3));
            foreach($response->getItems() as $item) {
                // echo $item->getDisplayName();
            }

            $response = $api->listPeople(array("user_id" => "me", "collection" => "visible", "max_results" => 3));
            foreach($response->getItems() as $item) {
                // echo $item->getDisplayName();
            }
        }
        
    }


Resources
-------------

Google Plus API Documentation - [https://developers.google.com/+/api/latest/](https://developers.google.com/+/api/latest/)

Language Codes - [https://developers.google.com/+/api/search#available-languages](https://developers.google.com/+/api/search#available-languages)