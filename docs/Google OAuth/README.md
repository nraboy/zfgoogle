Google OAuth 2.0
==============================

Google OAuth 2.0 classes for ZendFramework 2
   

Full OAuth Configuration (Not usually necessary)
-------------

```
#!php
protected $_config = array(
    "approval_prompt" => "force|auto",
    "grant_type" => "authorization_code|refresh_token",
    "response_type" => "code",
    "access_type" => "online|offline",
    "state" => "",
    "client_id" => "{CLIENT_ID_HERE}",
    "client_secret" => "{CLIENT_SECRET_HERE}",
    "redirect_uri" => "https://www.example.com/callback",
    "scope" => array(
        "https://www.googleapis.com/auth/userinfo.email",
        "https://www.googleapis.com/auth/userinfo.profile",
    )
);
```


Minimum OAuth Configuration
-------------

```
#!php
protected $_config = array(
    "client_id" => "{CLIENT_ID_HERE}",
    "client_secret" => "{CLIENT_SECRET_HERE}",
    "redirect_uri" => "https://www.example.com/callback",
    "scope" => array(
        "https://www.googleapis.com/auth/userinfo.email",
        "https://www.googleapis.com/auth/userinfo.profile",
    )
);
```

Expected Results for OAuth::getAccessToken($requestToken)
-------------

OAuth::getAccessToken($requestToken) will return a Google\OAuth\Tokens\AccessToken object
with the following methods:

```
#!text
Google\OAuth\Tokens\AccessToken::getAccessToken();
Google\OAuth\Tokens\AccessToken::getRefreshToken();
Google\OAuth\Tokens\AccessToken::getTokenType();
Google\OAuth\Tokens\AccessToken::getExpirationTime();
```

OAuth::getAccessToken($requestToken) should only be called once during the session.  To refresh an 
expired token OAuth::refreshAccessToken($refreshToken) should be called.


Expected Results for OAuth::refreshAccessToken($refreshToken)
-------------

OAuth::refreshAccessToken($refreshToken) will return a Google\OAuth\Tokens\RefreshToken object 
with the following methods:

```
#!text
Google\OAuth\Tokens\RefreshToken::getAccessToken();
Google\OAuth\Tokens\RefreshToken::getTokenType();
Google\OAuth\Tokens\RefreshToken::getExpirationTime();
```


Example
-------------

    use Google\OAuth\OAuth;

    class MyAuthController extends AbstractActionController {

        protected $_config;

        public function __construct() {
            $this->_config = array(
                "approval_prompt" => "force",
                "client_id" => "{CLIENT_ID_HERE}",
                "client_secret" => "{CLIENT_SECRET_HERE}",
                "redirect_uri" => "https://www.example.com/callback",
                "scope" => array(
                    "https://www.googleapis.com/auth/userinfo.email",
                    "https://www.googleapis.com/auth/userinfo.profile",
                )
            );
        }

        public function indexAction() {
            $googleAuth = new OAuth($this->_config);
            $googleAuth->getRequestToken();
        }

        public function callbackAction() {
            $requestToken = (String) $this->params()->fromQuery('code', 0);
            $googleAuth = new OAuth($this->_config, array("sslverifypeer" => false));
            $response = $googleAuth->getAccessToken($requestToken);
            // echo $response->getAccessToken();
            // echo $response->getRefreshToken();
        }
        
    }


Resources
-------------

Google OAuth 2.0 Documentation - [https://developers.google.com/accounts/docs/OAuth2WebServer](https://developers.google.com/accounts/docs/OAuth2WebServer)