Google Play Android Developer API
==============================

Google Play Android Developer API classes for ZendFramework 2


Required OAuth Scopes
-------------

```
#!text
https://www.googleapis.com/auth/androidpublisher
```


Required API Parameters
-------------

```
#!php
protected $_params = array(
    "package_name" => "{FULL_PACKAGE_NAME_HERE}",
    "subscription_id" => "{PURCHASE_OR_SUBSCRIPTION_ID_HERE}",
    "token" => "{TOKEN_FROM_ANDROID_DEVICE_HERE}"
);
```


Expected Results for AndroidDeveloper::getPurchase($params)
-------------

AndroidDeveloper::getPurchase($params) will return a Google\API\Responses\AndroidDeveloper object
with the following methods:

```
#!text
Google\API\Responses\AndroidDeveloper::getKind();
Google\API\Responses\AndroidDeveloper::getInitiationTimestamp();
Google\API\Responses\AndroidDeveloper::getExpirationTimestamp();
Google\API\Responses\AndroidDeveloper::getAutoRenew();
Google\API\Responses\AndroidDeveloper::getErrorCode();
Google\API\Responses\AndroidDeveloper::getErrorMessage();
```


Example
-------------

    use Google\API\AndroidDeveloper;

    class MyAPIController extends AbstractActionController {

        protected $_params;
        protected $_options;

        public function __construct() {
            $this->_params = array(
                "package_name" => "{FULL_PACKAGE_NAME_HERE}",
                "subscription_id" => "{PURCHASE_OR_SUBSCRIPTION_ID_HERE}",
                "token" => "{TOKEN_FROM_ANDROID_DEVICE_HERE}"
            );
            $this->_options = array(
                "sslverifypeer" => false
            );
        }

        public function indexAction() {
            $api = new AndroidDeveloper($accessToken, $this->_options);
            $response = $api->getPurchase($this->_params);
            // echo $response->getKind();
            // echo $response->getExpirationTimestamp();
            // echo $response->getErrorMessage();

            if($response->getErrorCode() == "") {
                $api->cancelPurchase($this->_params);
            }
        }
        
    }


Resources
-------------

Google Play Android Developer API Documentation - [https://developers.google.com/android-publisher/v1/](https://developers.google.com/android-publisher/v1/)