Google Analytics API
==============================

Google Analytics API classes for ZendFramework 2


Required OAuth Scopes
-------------

```
#!text
https://www.googleapis.com/auth/analytics
```


ZFGoogle Function to API Mapping
-------------

```
#!text
| ZFGoogle Function               | Google API Command |
| ------------------------------- | ------------------ |
| Google\API\Analytics::query()   | get                |
```


Possible API Parameters
-------------

```
#!php
protected $_params = array(
    "ids" => "{PROFILE_ID_HERE}",
    "metrics" => "{COMMA_SEPARATED_METRICS_HERE}",
    "dimensions" => "{COMMA_SEPARATED_DIMENSIONS_HERE}",
    "start_date" => "{YYYY-MM-DD}",
    "end_date" => "{YYYY-MM-DD}",
    "max_results" => "25",
    "sort" => "{COMMA_SEPARATED_SORT_HERE}",
    "filters" => "{COMMA_SEPARATED_FILTERS_HERE}",
    "segment" => "{SEGMENT_HERE}",
);
```

For help choosing the appropriate parameters for each API call, visit the official 
Google documentation found in the resources of this page


Expected Results for Analytics::query($params)
-------------

Analytics::query($params) will return a Google\API\Responses\Analytics object
with the following methods:

```
#!text
Google\API\Responses\Analytics::getKind();
Google\API\Responses\Analytics::getTotalResults();
Google\API\Responses\Analytics::getColumns();
Google\API\Responses\Analytics::getRows();
Google\API\Responses\Analytics::getErrorCode();
Google\API\Responses\Analytics::getErrorMessage();
```

Google\API\Responses\Analytics::getRows() will return an array of rows where each row is an associative array.  
The keys for each row are the dimension and metric names used which are also each of the columns found in 
Google\API\Responses\Analytics::getColumns()


Example
-------------

    use Google\API\Analytics;

    class MyAPIController extends AbstractActionController {

        protected $_options;
        protected $_params;

        public function __construct() {
            $this->_params = array(
                "ids" => "{PROFILE_ID_HERE}", 
                "metrics" => "ga:visitors", 
                "start_date" => "2013-12-25", 
                "end_date" => "2014-01-07", 
                "dimensions" => "ga:date,ga:year"
            );
            $this->_options = array(
                "sslverifypeer" => false
            );
        }

        public function indexAction() {
            $api = new Analytics($accessToken, $this->_options);
            $response = $api->query($this->_params);
            // echo $response->getKind();
            // echo $response->getTotalResults();

            foreach($response->getRows() as $row) {
                // echo $row["ga:date"];
                // echo $row["ga:year"];
                // echo $row["ga:visitors"];
            }
        }
        
    }


Resources
-------------

Google Analytics API Documentation - [https://developers.google.com/analytics/devguides/reporting/core/v3/reference](https://developers.google.com/analytics/devguides/reporting/core/v3/reference)

Metrics and Dimensions Reference - [https://developers.google.com/analytics/devguides/reporting/core/dimsmets](https://developers.google.com/analytics/devguides/reporting/core/dimsmets)